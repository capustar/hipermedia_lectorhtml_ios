//
//  ViewController.swift
//  LectorHtml
//
//  Created by Antonio Pertusa on 26/10/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button_go: UIBarButtonItem!
    
    
    @IBAction func connectUrl(_ sender: Any)
    {
        
        self.textField.resignFirstResponder() // Para ocultar el teclado
    
        // TODO: Realizar la conexión y mostrar el resultado de la url
        
        // 1. Crear la sesión
        
        // Creamos la configuración
        let config = URLSessionConfiguration.default
        // Creamos la sesión con esta configuración
        let session = URLSession(configuration: config)
        
        // 2. Crear URL para conectar a self.textField.text
        
        let url = self.textField.text!
        
        // 3- Codificar la URL
        if let encodedString = url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            let encodedUrl = URL(string: encodedString)!
            
            // 4. Crear petición a la URL
            let request = URLRequest(url: encodedUrl)
            
            // 5. Establecer conexión
            // 6. Guardar los resultados en el textView (si el texto es muy largo puede que no aparezca, pero no es un problema)
            
            session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                
                // La respuesta del servidor se recibe en este punto.
                // Se guardan los datos recibidos (data), la respuesta (response) y si ha habido algún error
                    
                DispatchQueue.main.async
                {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    self.button_go.isEnabled = false
                }
                sleep(3)
                
                if let error = error
                {
                    print(error.localizedDescription)
                }
                else
                {
                    let res = response as! HTTPURLResponse
                    
                    if res.statusCode == 200
                    {
                        DispatchQueue.main.async {
                            // Esperamos a que terminen de recibirse todos los datos
                            // Guardamos los datos en un string con formato ASCII o UTF8
                            let contents = String(data: data!, encoding: .ascii)!
                            self.textView.text = contents
                        }
                    }
                    else
                    {
                        print("Received status code: \(res.statusCode)")
                    }
                }
                
                DispatchQueue.main.async
                {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.button_go.isEnabled = true
                }
                
            }).resume() // Con esta instrucción lanzamos la petición asíncrona
            
        }
        
    }
    

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

